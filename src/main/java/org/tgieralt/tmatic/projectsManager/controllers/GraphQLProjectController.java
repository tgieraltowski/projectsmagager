package org.tgieralt.tmatic.projectsManager.controllers;

import graphql.ExecutionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.tgieralt.tmatic.projectsManager.services.GraphQLService;

@RestController
public class GraphQLProjectController {
    
    @Autowired
    private GraphQLService graphQLService;
    
    @PostMapping(value = "/graphql")
    public ResponseEntity<Object> allQueriesPost(@RequestBody String query) {
        ExecutionResult executionResult = graphQLService.getGraphQL().execute(query);
        return new ResponseEntity<>(executionResult, HttpStatus.OK);
    }
    
    @GetMapping(value = "/graphql")
    public ResponseEntity<Object> allQueriesGet(@RequestBody String query) {
        ExecutionResult executionResult = graphQLService.getGraphQL().execute(query);
        return new ResponseEntity<>(executionResult, HttpStatus.OK);
    }
}
