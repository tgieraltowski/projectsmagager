package org.tgieralt.tmatic.projectsManager;

import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.tgieralt.tmatic.projectsManager.models.Project;
import org.tgieralt.tmatic.projectsManager.models.Task;
import org.tgieralt.tmatic.projectsManager.models.User;
import org.tgieralt.tmatic.projectsManager.repositories.ProjectRepository;
import org.tgieralt.tmatic.projectsManager.repositories.TaskRepository;
import org.tgieralt.tmatic.projectsManager.repositories.UserRepository;

@SpringBootApplication
public class ProjectsManagerApplication implements CommandLineRunner{
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private UserRepository userRepository;
    
	public static void main(String[] args) {
		SpringApplication.run(ProjectsManagerApplication.class, args);
	}

    @Override
    public void run(String... args) throws Exception {
        projectRepository.deleteAll();
        taskRepository.deleteAll();
        userRepository.deleteAll();
        
        Project project1 = new Project("Super Project", "New awesome mega hiper project", LocalDate.of(2020, 3, 1), LocalDate.of(2020, 5, 20), "high");
        Project project2 = new Project("Evil Agenda", "Very evil project", LocalDate.of(2020, 5, 10), LocalDate.of(2021, 1, 3), " very high");
                
        projectRepository.save(project1);
        projectRepository.save(project2);
        
        Task task1 = new Task("Dominate the World!", "Seizing power in Poland will make me rule the whole world! Ha Ha Ha...", LocalDate.of(2020, 2, 29), LocalDate.of(2020, 5, 10), "extremely high", project2);
        task1.setProject(project2);
        
        taskRepository.save(task1);
        projectRepository.save(project2);
        
        Task task2 = new Task("Win elections", "Win presidential elections in Poland vs PAD", LocalDate.of(2020, 3, 12), LocalDate.of(2020, 5, 15), "medium", project2);
        task1.addChildTask(task2);
        taskRepository.save(task2);
        taskRepository.save(task1);
        
        project2.removeTask(task1);
        project2.addTask(task1);
        project2.addTask(task2);        
        projectRepository.save(project2);
        
        project1.addProject(project2);
        projectRepository.save(project1);
        projectRepository.save(project2);
        
        User user1 = new User("Tomasz", "Gieraltowski", "tgieraltowski@gmail.com", LocalDate.of(1984, 4, 27));
        userRepository.save(user1);
        
        task2.addUser(user1);
        taskRepository.save(task2);
        userRepository.save(user1);
        project2.removeTask(task2);
        project2.addTask(task2);
        project2.addUser(user1);
        userRepository.save(user1);
        projectRepository.save(project2);
        
        Project project3 = new Project("Early Project", "This project was created long ago", LocalDate.of(2001, 1, 1), LocalDate.of(2007, 5, 12), "Low");
        projectRepository.save(project3);
        
        Project project4 = new Project("Late Project", "This project will be created in the future", LocalDate.of(2100, 11, 11), LocalDate.of(2120, 7, 17), "Medium");
        projectRepository.save(project4);
        
        
        for (Project project : projectRepository.findAll()) {
            System.out.println(project);
        }
        
        for (Task task : taskRepository.findAll()) {
            System.out.println(task);
        }
    }

}
