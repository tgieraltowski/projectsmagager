package org.tgieralt.tmatic.projectsManager.services;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import java.io.File;
import java.io.IOException;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.tgieralt.tmatic.projectsManager.graphQL.DeleteProjectDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.DeleteTaskDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.DeleteUserDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.NewProjectDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.NewTaskDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.NewUserDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.ProjectByTitleDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.ProjectListDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.ProjectListSortedByCreationDateDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.ProjectListSortedByTitleDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.TaskListDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.TaskListSortedByCreationDateDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.TaskListSortedByTitleDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.UpdateProjectDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.UpdateTaskDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.UpdateUserDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.UserByEmailDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.UserBySurnameDataFetcher;
import org.tgieralt.tmatic.projectsManager.graphQL.UserListDataFetcher;

@Service
public class GraphQLService {
    @Value("classpath:tmatic.graphql")
    Resource schemaResource;
    
    @Autowired
    private ProjectListDataFetcher projectListDataFetcher;
    @Autowired
    private TaskListDataFetcher taskListDataFetcher;
    @Autowired
    private UserListDataFetcher userListDataFetcher;
    @Autowired
    private ProjectByTitleDataFetcher projectByTitleDataFetcher;
    @Autowired
    private UserByEmailDataFetcher userByEmailDataFetcher;
    @Autowired
    private UserBySurnameDataFetcher userBySurnameDataFetcher;
    @Autowired
    private ProjectListSortedByCreationDateDataFetcher projectListSortedByCreationDateDataFetcher;
    @Autowired
    private ProjectListSortedByTitleDataFetcher projectListSortedByTitleDataFetcher;
    @Autowired
    private TaskListSortedByCreationDateDataFetcher taskListSortedByCreationDateDataFetcher;
    @Autowired
    private TaskListSortedByTitleDataFetcher taskListSortedByTitleDataFetcher;
    @Autowired
    private NewProjectDataFetcher newProjectDataFetcher;
    @Autowired
    private NewTaskDataFetcher newTaskDataFetcher;
    @Autowired
    private NewUserDataFetcher newUserDataFetcher;
    @Autowired
    private DeleteProjectDataFetcher deleteProjectDataFetcher;
    @Autowired
    private DeleteTaskDataFetcher deleteTaskDataFetcher;
    @Autowired
    private DeleteUserDataFetcher deleteUserDataFetcher;
    @Autowired
    private UpdateProjectDataFetcher updateProjectDataFetcher;
    @Autowired
    private UpdateTaskDataFetcher updateTaskDataFetcher;
    @Autowired
    private UpdateUserDataFetcher updateUserDataFetcher;
    
    private GraphQL graphQL;
    
    @PostConstruct
    public void loadGraphQLSchema() throws IOException {
        File schema = schemaResource.getFile();
        TypeDefinitionRegistry typeDefinitionRegistry = new SchemaParser().parse(schema);
        RuntimeWiring runtimeWiring = initWiring();
        GraphQLSchema graphQLSchema = new SchemaGenerator().makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);
        
        graphQL = GraphQL.newGraphQL(graphQLSchema).build();
    }

    private RuntimeWiring initWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher("allProjects", projectListDataFetcher)
                        .dataFetcher("projectByTitle", projectByTitleDataFetcher)
                        .dataFetcher("allProjectsSortedByCreationDate", projectListSortedByCreationDateDataFetcher)
                        .dataFetcher("allProjectsSortedByTitle", projectListSortedByTitleDataFetcher)
                        .dataFetcher("allTasks", taskListDataFetcher)
                        .dataFetcher("allTasksSortedByCreationDate", taskListSortedByCreationDateDataFetcher)
                        .dataFetcher("allTasksSortedByTitle", taskListSortedByTitleDataFetcher)
                        .dataFetcher("allUsers", userListDataFetcher)
                        .dataFetcher("userBySurname", userBySurnameDataFetcher)
                        .dataFetcher("userByEmail", userByEmailDataFetcher))
                .type("Mutation", typeWiring -> typeWiring
                        .dataFetcher("newProject", newProjectDataFetcher)
                        .dataFetcher("newTask", newTaskDataFetcher)
                        .dataFetcher("newUser", newUserDataFetcher)
                        .dataFetcher("deleteProject", deleteProjectDataFetcher)
                        .dataFetcher("deleteTask", deleteTaskDataFetcher)
                        .dataFetcher("deleteUser", deleteUserDataFetcher)
                        .dataFetcher("updateProject", updateProjectDataFetcher)
                        .dataFetcher("updateTask", updateTaskDataFetcher)
                        .dataFetcher("updateUser", updateUserDataFetcher))
                .build();
    }
    
    public GraphQL getGraphQL() {
        return graphQL;
    }
}
