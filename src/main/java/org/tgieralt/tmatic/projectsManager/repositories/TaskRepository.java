package org.tgieralt.tmatic.projectsManager.repositories;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.tgieralt.tmatic.projectsManager.models.Task;

public interface TaskRepository extends MongoRepository<Task, String>{

    public List<Task> findByTitle(String taskTitle);
    
}
