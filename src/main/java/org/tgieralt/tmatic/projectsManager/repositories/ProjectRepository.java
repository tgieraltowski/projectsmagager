package org.tgieralt.tmatic.projectsManager.repositories;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.tgieralt.tmatic.projectsManager.models.Project;

public interface ProjectRepository extends MongoRepository<Project, String>{

    public List<Project> findByTitle(String title);
    
}
