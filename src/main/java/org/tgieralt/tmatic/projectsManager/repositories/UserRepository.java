package org.tgieralt.tmatic.projectsManager.repositories;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.tgieralt.tmatic.projectsManager.models.User;

public interface UserRepository extends MongoRepository<User, String>{

    public Optional<User> findByEmail(String email);

    public Optional<User> findBySurname(String surname);
    
    public Optional<User> findByNameAndSurnameAndEmail(String name, String surname, String email);
    
}
