package org.tgieralt.tmatic.projectsManager.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(value = "project")
public class Project {
    @Id
    private String id;
    private String title;
    private String description;
    private LocalDate creationDate;
    private LocalDate startDate;
    private LocalDate endDate;
    private String priority;
    @DBRef(lazy = true)
    private List<User> users = new ArrayList<>();
    private List<String> connectedProjects_id = new ArrayList<>();
    @DBRef(lazy = true)
    private List<Task> tasks = new ArrayList<>();

    public Project() {
    }

    public Project(String title, String description, LocalDate startDate, LocalDate endDate, String priority) {
        this.title = title;
        this.description = description;
        this.creationDate = LocalDate.now();
        this.startDate = startDate;
        this.endDate = endDate;
        this.priority = priority;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public List<User> getUsers() {
        return users;
    }
    
    public void addUser(User user) {
        this.users.add(user);
        user.addProject(this);
    }
    
    public void removeUser(User user) {
        this.users.remove(user);
        user.removeProject(this);
    }

    public List<String> getConnectedProjectsId() {
        return connectedProjects_id;
    }
    
    public void addProject(Project project) {
        if (!connectedProjects_id.contains(project.getId())) {
            this.connectedProjects_id.add(project.getId());
            if (!project.getConnectedProjectsId().contains(this.id)) {
                project.addProject(this);
            }
        }
    }
    
    public void removeProject(Project project) {
        this.connectedProjects_id.remove(project.getId());
        if (project.getConnectedProjectsId().contains(this.id)) {
                project.removeProject(this);
            }
    }

    public List<Task> getTasks() {
        return tasks;
    }
    
    public void addTask(Task task) {
        this.tasks.add(task);
    }
    
    public void removeTask(Task task) {
        this.tasks.remove(task);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.id);
        hash = 71 * hash + Objects.hashCode(this.title);
        hash = 71 * hash + Objects.hashCode(this.description);
        hash = 71 * hash + Objects.hashCode(this.creationDate);
        hash = 71 * hash + Objects.hashCode(this.startDate);
        hash = 71 * hash + Objects.hashCode(this.endDate);
        hash = 71 * hash + Objects.hashCode(this.priority);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Project other = (Project) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.priority, other.priority)) {
            return false;
        }
        if (!Objects.equals(this.creationDate, other.creationDate)) {
            return false;
        }
        if (!Objects.equals(this.startDate, other.startDate)) {
            return false;
        }
        if (!Objects.equals(this.endDate, other.endDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Project{" + "id=" + id + ", title=" + title + ", description=" + description + ", creationDate=" + creationDate + ", startDate=" + startDate + ", endDate=" + endDate + ", priority=" + priority + ", connectedProjects=" + connectedProjects_id + '}';
    }

       
    
}
