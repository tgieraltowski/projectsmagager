package org.tgieralt.tmatic.projectsManager.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(value = "task")
public class Task {
    @Id
    private String id;
    private String title;
    private String content;
    private LocalDate creationDate;
    private LocalDate startDate;
    private LocalDate endDate;
    private String priority;
    @DBRef(lazy = true)
    private Task parentTask;
    @DBRef(lazy = true)
    private List<Task> childTasks = new ArrayList<>();
    @DBRef(lazy = true)
    private List<User> users = new ArrayList<>();
    @DBRef(lazy = true)
    private Project project;

    public Task() {
    }

    public Task(String title, String content, LocalDate startDate, LocalDate endDate, String priority, Project parentProject) {
        this.title = title;
        this.content = content;
        this.creationDate = LocalDate.now();
        this.startDate = startDate;
        this.endDate = endDate;
        this.priority = priority;
        this.project = parentProject;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Project getProject_id() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
        project.addTask(this);
    }

    public Task getParentTask() {
        return parentTask;
    }

    public void setParentTask(Task parentTask) {
        this.parentTask = parentTask;
    }    

    public List<Task> getChildTasks() {
        return childTasks;
    }
    
    public void addChildTask(Task task) {
        this.childTasks.add(task);
        task.setParentTask(this);
    }
    
    public void removeChildTask(Task task) {
        this.childTasks.remove(task);
        task.setParentTask(null);
    }

    public List<User> getUsers() {
        return users;
    }
    
    public void addUser(User user) {
        this.users.add(user);
        user.addTask(this);        
    }
    
    public void removeUser(User user) {
        this.users.remove(user);
        user.removeTask(this);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.id);
        hash = 11 * hash + Objects.hashCode(this.title);
        hash = 11 * hash + Objects.hashCode(this.content);
        hash = 11 * hash + Objects.hashCode(this.creationDate);
        hash = 11 * hash + Objects.hashCode(this.startDate);
        hash = 11 * hash + Objects.hashCode(this.endDate);
        hash = 11 * hash + Objects.hashCode(this.priority);
        hash = 11 * hash + Objects.hashCode(this.project);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Task other = (Task) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.content, other.content)) {
            return false;
        }
        if (!Objects.equals(this.priority, other.priority)) {
            return false;
        }
        if (!Objects.equals(this.creationDate, other.creationDate)) {
            return false;
        }
        if (!Objects.equals(this.startDate, other.startDate)) {
            return false;
        }
        if (!Objects.equals(this.endDate, other.endDate)) {
            return false;
        }
        if (!Objects.equals(this.project, other.project)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Task{" + "id=" + id + ", title=" + title + ", content=" + content + ", creationDate=" + creationDate + ", startDate=" + startDate + ", endDate=" + endDate + ", priority=" + priority + ", users=" + users + '}';
    }
    
    
}
