package org.tgieralt.tmatic.projectsManager.graphQL;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.time.LocalDate;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tgieralt.tmatic.projectsManager.models.Project;
import org.tgieralt.tmatic.projectsManager.repositories.ProjectRepository;

@Component
public class NewProjectDataFetcher implements DataFetcher<Project>{
    @Autowired
    private ProjectRepository projectRepository;
    
    @Override
    public Project get(DataFetchingEnvironment dfe) {
        Map<String, String> projectInput = dfe.getArgument("project");
        String title = projectInput.get("title");
        String description = projectInput.get("description");
        LocalDate startDate = LocalDate.parse(projectInput.get("startDate"));
        LocalDate endDate = LocalDate.parse(projectInput.get("endDate"));
        String priority = projectInput.get("priority");
        Project project = new Project(title, description, startDate, endDate, priority);
        projectRepository.save(project);
        return project;
    }
    
}
