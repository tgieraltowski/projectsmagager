package org.tgieralt.tmatic.projectsManager.graphQL;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tgieralt.tmatic.projectsManager.models.Project;
import org.tgieralt.tmatic.projectsManager.models.Task;
import org.tgieralt.tmatic.projectsManager.models.User;
import org.tgieralt.tmatic.projectsManager.repositories.ProjectRepository;
import org.tgieralt.tmatic.projectsManager.repositories.TaskRepository;
import org.tgieralt.tmatic.projectsManager.repositories.UserRepository;

@Component
public class UpdateTaskDataFetcher implements DataFetcher<Task>{
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private UserRepository userRepository;
    
    @Override
    public Task get(DataFetchingEnvironment dfe) {
        String taskId = dfe.getArgument("id");
        String title = dfe.getArgument("title");
        String content = dfe.getArgument("content");
        String startDate = dfe.getArgument("startDate");
        String endDate = dfe.getArgument("endDate");
        String priority = dfe.getArgument("priority");
        String addChildTask_id = dfe.getArgument("addChildTask_id");
        String addUser_id = dfe.getArgument("addUser_id");
        
        Task task = taskRepository.findById(taskId).orElse(null);
        if (task == null) {
            return null;
        }
        
        if (title != null) {
            task.setTitle(title);
        }
        if (content != null) {
            task.setContent(content);
        }
        if (startDate != null) {
            task.setStartDate(LocalDate.parse(startDate));
        }
        if (endDate != null) {
            task.setEndDate(LocalDate.parse(endDate));
        }
        if (priority != null) {
            task.setPriority(priority);
        }
        if (addChildTask_id != null) {
            Task childTask = taskRepository.findById(addChildTask_id).orElse(null);
            if (childTask != null) {
                task.addChildTask(childTask);
                taskRepository.save(childTask);
            }
        }
        if (addUser_id != null) {
            User user = userRepository.findById(addUser_id).orElse(null);
            if (user != null) {
                task.addUser(user);
                Project project = task.getProject_id();
                if (!project.getUsers().contains(user)) {
                    project.addUser(user);
                    projectRepository.save(project);
                }
                userRepository.save(user);
            }
        }
        taskRepository.save(task);
        return task;
    }
    
}
