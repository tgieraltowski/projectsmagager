package org.tgieralt.tmatic.projectsManager.graphQL;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tgieralt.tmatic.projectsManager.models.User;
import org.tgieralt.tmatic.projectsManager.repositories.UserRepository;

@Component
public class UpdateUserDataFetcher implements DataFetcher<User>{
    @Autowired
    private UserRepository userRepository;
    
    @Override
    public User get(DataFetchingEnvironment dfe) {
        String userId = dfe.getArgument("id");
        String name = dfe.getArgument("name");
        String surname = dfe.getArgument("surname");
        String email = dfe.getArgument("email");
        String birthDate = dfe.getArgument("birthDate");
        
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            return null;
        }
        
        if (name != null) {
            user.setName(name);
        }
        if (surname != null) {
            user.setSurname(surname);
        }
        if (email != null) {
            user.setEmail(email);
        }
        if (birthDate != null) {
            user.setBirtDate(LocalDate.parse(birthDate));
        }
        userRepository.save(user);
        return user;
    }
    
}
