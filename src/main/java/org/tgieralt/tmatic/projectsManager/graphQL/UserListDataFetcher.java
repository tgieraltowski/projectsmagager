package org.tgieralt.tmatic.projectsManager.graphQL;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tgieralt.tmatic.projectsManager.models.User;
import org.tgieralt.tmatic.projectsManager.repositories.UserRepository;

@Component
public class UserListDataFetcher implements DataFetcher<List<User>>{
    @Autowired
    private UserRepository userRepository;
    
    @Override
    public List<User> get(DataFetchingEnvironment dfe) {
        return userRepository.findAll();
    }
    
}
