package org.tgieralt.tmatic.projectsManager.graphQL;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tgieralt.tmatic.projectsManager.models.Project;
import org.tgieralt.tmatic.projectsManager.repositories.ProjectRepository;

@Component
public class ProjectListSortedByTitleDataFetcher implements DataFetcher<List<Project>>{
    @Autowired
    private ProjectRepository projectRepository;
    
    @Override
    public List<Project> get(DataFetchingEnvironment dfe) {
        List<Project> allProjects = projectRepository.findAll();
        allProjects.sort((Project o1, Project o2) -> o1.getTitle().compareTo(o2.getTitle()));
        return allProjects;
    }
    
}
