package org.tgieralt.tmatic.projectsManager.graphQL;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tgieralt.tmatic.projectsManager.models.Project;
import org.tgieralt.tmatic.projectsManager.models.Task;
import org.tgieralt.tmatic.projectsManager.models.User;
import org.tgieralt.tmatic.projectsManager.repositories.ProjectRepository;
import org.tgieralt.tmatic.projectsManager.repositories.TaskRepository;
import org.tgieralt.tmatic.projectsManager.repositories.UserRepository;

@Component
public class DeleteTaskDataFetcher implements DataFetcher<Boolean>{
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private UserRepository userRepository;
    
    @Override
    public Boolean get(DataFetchingEnvironment dfe) {
        Map<String, String> deleteInput = dfe.getArgument("where");
        String taskId = deleteInput.getOrDefault("id", null);
        String taskTitle = deleteInput.getOrDefault("title", null);
        Task task;
        if (taskId != null) {
            task = taskRepository.findById(taskId).get();
        } else {
            if (taskRepository.findByTitle(taskTitle).size() > 0) {
            task = taskRepository.findByTitle(taskTitle).get(0);
            } else {
                return false;
            }
        }
        Project project = task.getProject_id();
        List<Task> childTasks = task.getChildTasks();
        List<User> users = task.getUsers();
        
        project.getTasks().remove(task);        
        for (Task childTask : childTasks) {
            project.getTasks().remove(childTask);
            taskRepository.delete(childTask);
        }
        projectRepository.save(project);
        for (User user : users) {
            user.getTasks().remove(task);
            user.getTasks().removeAll(childTasks);
            userRepository.save(user);
        }
        taskRepository.delete(task);
        
        if (taskId != null && taskRepository.findById(taskId).isPresent()) {
            return false;
        } else if (taskTitle != null && taskRepository.findByTitle(taskTitle).size() > 0) {
            return false;
        }
        return true;
    }
    
}
