package org.tgieralt.tmatic.projectsManager.graphQL;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tgieralt.tmatic.projectsManager.models.Task;
import org.tgieralt.tmatic.projectsManager.repositories.TaskRepository;

@Component
public class TaskListSortedByCreationDateDataFetcher implements DataFetcher<List<Task>>{
    @Autowired
    private TaskRepository taskRepository;
    
    @Override
    public List<Task> get(DataFetchingEnvironment dfe) {
        List<Task> allTasks = taskRepository.findAll();
        allTasks.sort(new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getCreationDate().compareTo(o2.getCreationDate());
            }
        });
        return allTasks;
    }
    
}
