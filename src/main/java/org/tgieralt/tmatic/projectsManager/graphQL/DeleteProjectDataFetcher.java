
package org.tgieralt.tmatic.projectsManager.graphQL;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tgieralt.tmatic.projectsManager.models.Project;
import org.tgieralt.tmatic.projectsManager.models.Task;
import org.tgieralt.tmatic.projectsManager.models.User;
import org.tgieralt.tmatic.projectsManager.repositories.ProjectRepository;
import org.tgieralt.tmatic.projectsManager.repositories.TaskRepository;
import org.tgieralt.tmatic.projectsManager.repositories.UserRepository;

@Component
public class DeleteProjectDataFetcher implements DataFetcher<Boolean>{
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private UserRepository userRepository;
    
    @Override
    public Boolean get(DataFetchingEnvironment dfe) {
        Map<String, String> deleteInput = dfe.getArgument("where");
        String projectId = deleteInput.getOrDefault("id", null);
        String projetTitle = deleteInput.getOrDefault("title", null);
        Project project;
        if (projectId != null) {
            project = projectRepository.findById(projectId).get();
        } else {
            if (projectRepository.findByTitle(projetTitle).size() > 0) {
            project = projectRepository.findByTitle(projetTitle).get(0);
            } else {
                return false;
            }
        }
        
        List<Task> tasks = project.getTasks();
        List<User> users = project.getUsers();
        for (Task task : tasks) {
            taskRepository.delete(task);
        }
        for (User user : users) {
            user.getProjects().remove(project);
            user.getTasks().removeAll(tasks);
            userRepository.save(user);
        }
        projectRepository.delete(project);
        
        if (projectId != null && projectRepository.findById(projectId).isPresent()) {
            return false;
        } else if (projetTitle != null && projectRepository.findByTitle(projetTitle).size() > 0) {
            return false;
        }
        return true;
    }    
}
