package org.tgieralt.tmatic.projectsManager.graphQL;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tgieralt.tmatic.projectsManager.models.Project;
import org.tgieralt.tmatic.projectsManager.repositories.ProjectRepository;

@Component
public class UpdateProjectDataFetcher implements DataFetcher<Project>{
    @Autowired
    private ProjectRepository projectRepository;
    
    @Override
    public Project get(DataFetchingEnvironment dfe) {
        String projectId = dfe.getArgument("id");
        String title = dfe.getArgument("title");
        String description = dfe.getArgument("description");
        String startDate = dfe.getArgument("startDate");
        String endDate = dfe.getArgument("endDate");
        String priority = dfe.getArgument("priority");
        String connectProject_id = dfe.getArgument("connectProject_id");
        
        Project project = projectRepository.findById(projectId).orElse(null);
        if (project == null) {
            return null;
        }
        
        if (title != null) {
            project.setTitle(title);
        }
        if (description != null) {
            project.setDescription(description);
        }
        if (startDate != null) {
            project.setStartDate(LocalDate.parse(startDate));
        }
        if (endDate != null) {
            project.setEndDate(LocalDate.parse(endDate));
        }
        if (priority != null) {
            project.setPriority(priority);
        }
        if (connectProject_id != null) {
            Project project2 = projectRepository.findById(connectProject_id).orElse(null);
            if (project2 != null) {
                project2.addProject(project);
                projectRepository.save(project2);
            }
        }
        projectRepository.save(project);
        return project;
    }
    
}
