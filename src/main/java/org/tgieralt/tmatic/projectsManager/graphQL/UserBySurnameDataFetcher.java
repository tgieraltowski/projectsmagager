package org.tgieralt.tmatic.projectsManager.graphQL;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tgieralt.tmatic.projectsManager.models.User;
import org.tgieralt.tmatic.projectsManager.repositories.UserRepository;

@Component
public class UserBySurnameDataFetcher implements DataFetcher<Optional<User>>{
    @Autowired
    private UserRepository userRepository;
    
    @Override
    public Optional<User> get(DataFetchingEnvironment dfe) {
        String surname = dfe.getArgument("surname");
        return userRepository.findBySurname(surname);
    }
    
}
