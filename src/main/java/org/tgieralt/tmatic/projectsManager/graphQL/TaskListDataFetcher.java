package org.tgieralt.tmatic.projectsManager.graphQL;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tgieralt.tmatic.projectsManager.models.Task;
import org.tgieralt.tmatic.projectsManager.repositories.TaskRepository;

@Component
public class TaskListDataFetcher implements DataFetcher<List<Task>>{
    @Autowired
    private TaskRepository taskRepository;
    
    @Override
    public List<Task> get(DataFetchingEnvironment dfe) {
        return taskRepository.findAll();
    }
    
}
