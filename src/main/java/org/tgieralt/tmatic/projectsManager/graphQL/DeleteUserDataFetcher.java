package org.tgieralt.tmatic.projectsManager.graphQL;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tgieralt.tmatic.projectsManager.models.Project;
import org.tgieralt.tmatic.projectsManager.models.Task;
import org.tgieralt.tmatic.projectsManager.models.User;
import org.tgieralt.tmatic.projectsManager.repositories.ProjectRepository;
import org.tgieralt.tmatic.projectsManager.repositories.TaskRepository;
import org.tgieralt.tmatic.projectsManager.repositories.UserRepository;

@Component
public class DeleteUserDataFetcher implements DataFetcher<Boolean>{
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private UserRepository userRepository;
    
    @Override
    public Boolean get(DataFetchingEnvironment dfe) {
        Map<String, String> deleteInput = dfe.getArgument("where");
        String userId = deleteInput.getOrDefault("id", null);
        String name = deleteInput.getOrDefault("name", null);
        String surname = deleteInput.getOrDefault("surname", null);
        String email = deleteInput.getOrDefault("email", null);
        User user = null;
        
        if (userId != null) {
            user = userRepository.findById(userId).orElse(null);
        } else if (name != null && surname != null && email != null) {
            user = userRepository.findByNameAndSurnameAndEmail(name, surname, email).orElse(null);
        }
        if (user == null) {
            return false;
        }
        
        List<Project> projects = user.getProjects();
        List<Task> tasks = user.getTasks();
        for (Project project : projects) {
            project.getUsers().remove(user);
            projectRepository.save(project);
        }
        for (Task task : tasks) {
            task.getUsers().remove(user);
            taskRepository.save(task);
        }
        userRepository.delete(user);
        
        if (taskRepository.findById(user.getId()).isPresent()) {
            return false;
        }
        return true;
    }
    
}
