
package org.tgieralt.tmatic.projectsManager.graphQL;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.time.LocalDate;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tgieralt.tmatic.projectsManager.models.Project;
import org.tgieralt.tmatic.projectsManager.models.Task;
import org.tgieralt.tmatic.projectsManager.repositories.ProjectRepository;
import org.tgieralt.tmatic.projectsManager.repositories.TaskRepository;

@Component
public class NewTaskDataFetcher implements DataFetcher<Task>{
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private ProjectRepository projectRepository;
    
    @Override
    public Task get(DataFetchingEnvironment dfe) {
        Map<String, String> taskInput = dfe.getArgument("task");
        String title = taskInput.get("title");
        String content = taskInput.get("content");
        LocalDate startDate = LocalDate.parse(taskInput.get("startDate"));
        LocalDate endDate = LocalDate.parse(taskInput.get("endDate"));
        String priority = taskInput.get("priority");
        String projectId = taskInput.get("project");
        Project project = projectRepository.findById(projectId).orElse(projectRepository.findByTitle(projectId).get(0));
        String parentTask = taskInput.getOrDefault("task", null);
        Task task = new Task(title, content, startDate, endDate, priority, project);
        if (parentTask != null) {
            Task foundParentTask = taskRepository.findById(parentTask).orElse(null);
            if (foundParentTask != null) {
                foundParentTask.addChildTask(task);
                taskRepository.save(foundParentTask);
            }
        }
        project.addTask(task);        
        taskRepository.save(task);
        projectRepository.save(project);
        return task;
    }
    
}
