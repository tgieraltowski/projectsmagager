package org.tgieralt.tmatic.projectsManager.graphQL;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import java.time.LocalDate;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tgieralt.tmatic.projectsManager.models.User;
import org.tgieralt.tmatic.projectsManager.repositories.UserRepository;

@Component
public class NewUserDataFetcher implements DataFetcher<User>{
    @Autowired
    private UserRepository userRepository;
    
    @Override
    public User get(DataFetchingEnvironment dfe) {
        Map<String, String> newUserInput = dfe.getArgument("user");
        String name = newUserInput.get("name");
        String surname = newUserInput.get("surname");
        String email = newUserInput.get("email");
        LocalDate birthDate = LocalDate.parse(newUserInput.get("birthDate"));
        User user = new User(name, surname, email, birthDate);
        userRepository.save(user);
        return user;
    }
    
}
